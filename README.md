# All about DICOM artery segmentation #

1. detection
2. classification
3. manual segmentation
4. auto segmentation
5. auto segmentation by using DNN

### What is this repository for? ###

this repo is made for studying image segmentation

### How do I get set up? ###

* Python 3.8.5, openCV - cv2
* Configuration  - 모름
* Dependencies  - numpy, glob 기타등등 openCV 라이브러리
* Database configuration - 모름
* How to run tests - visual studio code 
* Deployment instructions

### Contribution guidelines ###

* Writing tests - temporal draft -> png to vol -> amend temporal -> png to vol 이 과정을 반복
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin - Joy , Daesoo
* Other community or team contact